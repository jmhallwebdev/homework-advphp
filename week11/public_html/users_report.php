<?php
// need the session so we can check the users access
session_start();

require_once('../inc/users.class.php');

// need instance of users to check access
$users = new users();

// if there is no user saved in session (ie no login) or the user 
// doesnt have access, kick back to list page.
// go to user_login page first.
if (!isset($_SESSION['userID']) || !$users->userHasAccess($_SESSION['userID'], "4"))
{
    header("location: users_list.php");
    exit;
}

// create an instance of the news article class
$user = new users();

// setup our paging, default 1 or or use what is in the GET parameter
if (isset($_GET['pageNumber']) && $_GET['pageNumber'] > 1)
{
    $pageNumber = $_GET['pageNumber'];
}
else
{
    $pageNumber = 1;
}

// default to an empty list
$usersList = array();

// check to see if button was click
if (isset($_GET['usersFilter']))
{
    // run report
    $usersList = $user->runReport($_GET, $pageNumber);
}


include('../tpl/users_report.tpl.php');
?>