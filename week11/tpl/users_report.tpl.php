<html>
    <body>
        <form action="<?php echo $_SERVER['SCRIPT_NAME']; ?>" method="GET">
            User: <input type="text" name="usersFilter"/><br>
            <input type="submit" name="btnViewReport" value="View Report"/>
        </form>
        
        <a href="users_download.php?usersFilter=<?php echo $_GET['usersFilter']; ?>">Download</a><br>
        
        <?php if (count($usersList) > 0) // if we have articles to show
        { ?>
        <table border="1">
            <header>
                <tr>
                    <th>
                       userID
                    </th>
                    <th>
                       userName
                    </th>
                    <th>
                       userLevel
                    </th>
                </tr>
            </header>
            <?php foreach ($usersList as $currentUserData) // loop through each article
            { ?>
            <tr>            
                <td>
                    <?php echo $currentUserData['userID']; ?>
                </td>
                <td>
                    <?php echo $currentUserData['userName']; ?>
                </td>
                <td>
                    <?php echo $currentUserData['userLevel']; ?>
                </td>
            </tr>
            <?php } ?>
        </table>
        <?php } ?>
        <div>
            <?php if ($pageNumber > 1) // if this is not page one, show previous
            { ?>
                <a href="<?php echo $_SERVER['SCRIPT_NAME']; ?>?pageNumber=<?php echo ($pageNumber - 1); ?>&usersFilter=<?php echo $_GET['usersFilter']; ?>">Previous</a>
            <?php } ?>
            <?php if ($pageNumber >= 1 && count($usersList) > 0) // if this is page one or greater, show next
            { ?>
                <a href="<?php echo $_SERVER['SCRIPT_NAME']; ?>?pageNumber=<?php echo ($pageNumber + 1); ?>&usersFilter=<?php echo $_GET['usersFilter']; ?>">Next</a>
            <?php } ?>
        </div>
    </body>    
</html>