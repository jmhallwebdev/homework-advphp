<html>
    <a href="users_edit.php">Add User</a>
    <table border="1">
        <header>
            <tr>
                <th>
                   User Name
                </th>

<!--             see Garitt's week 8 files for function getUserLevels
 -->
                <th>
                   User Level
                </th>
                <th>
                   View
                </th>
                <th>
                   Edit
                </th>
            </tr>
        </header>
        <?php foreach ($userList as $currentUserData) 
        { ?>
        <tr>            
            <td>
                <?php echo $currentUserData['userName']; ?>
            </td>
            <td>
                <?php echo $userLevels[$currentUserData['userLevel']]; ?>
            </td>
            <td>
                <a href="users_view.php?userID=<?php echo $currentUserData['userID']; ?>">View</a>
            </td>
            <td>
                <a href="users_edit.php?userID=<?php echo $currentUserData['userID']; ?>">Edit</a>
            </td>
        </tr>
        <?php } ?>
    </table>
    <a href="users_report.php">Run User Report</a>
</html>
