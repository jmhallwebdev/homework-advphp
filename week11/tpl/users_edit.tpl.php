<html>
    <div>
        <span class=""><?php echo (isset($formData['userID']) && $formData['userID'] > 0 ? "Edit" : "Add" ); ?></span> Users
    </div>
    
    <ul>
        <?php foreach ($errors as $errorMsg) 
        { ?>
        <li><?php echo $errorMsg; ?></li>
        <?php } ?>        
    </ul>
    
    <form method="post" action="<?php echo $_SERVER['SCRIPT_NAME']; ?>" enctype="multipart/form-data">
        <input type="hidden" name="userID" value="<?php echo (isset($formData['userID']) ? $formData['userID'] : "" ); ?>"/><br>
        User Name: <input type="text" name="userName" value="<?php echo (isset($formData['userName']) ? $formData['userName'] : "" ); ?>"/><br>
        User Password: <input type="password" name="userPassword" value="<?php echo (isset($formData['userPassword']) ? $formData['userPassword'] : "" ); ?>"/><br>

        <!-- (below) swap this out for a dropdown, see Garritt' week 8 files -->

        User Level: 
        <select name="userLevel">
            <?php foreach ($userLevels as $userLevel => $levelDescription) 
            { ?>
                <option value="<?php echo $userLevel; ?>" <?php echo (isset($formData['userLevel']) && $formData['userLevel'] == $userLevel ? "selected" : "" ); ?>><?php echo $levelDescription; ?></option>
            <?php } ?>
        </select>
        User Image: <input type="file" name="userImage"/><br>
        
        <input type="submit" value="Save" name="btnSave"/>
        <input type="submit" value="Cancel" name="btnCancel"/>
    </form>    
</html>