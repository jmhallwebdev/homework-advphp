<?php
require_once('../inc/mydb.config.php');

class users 
{
    
    var $data = array(); // stores the article data 
    var $errors = array(); // stores error information
    var $db = null; // stores a pdo connection

    function __construct() 
    {
        $this->connectToDB();
    }
    
    function connectToDB()
    {
        $isConnected = true;
        
        // constructor of PDO always throws an exception
        try {
            $this->db = new PDO(DATABASE_CONNECTION_STRING, DATABASE_USER, DATABASE_PASS, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING));
        } 
        catch (PDOException $e)
        {
            $this->errors[] = 'Failed to connect to database';
            $this->errors[] = $e->getMessage();
            $isConnected = false;
        } 
        
        return $isConnected;
    }
    
    // data array can come from anywhere, but it has to adhere to the naming convention
    // and its keys must be the same names as the database columns.
    // ex. $dataArray['articleID'], $dataArray['articleTitle']
    function set($dataArray)
    {
        $this->data = $dataArray;
    }
    
    // data array can come from anywhere, but it has to adhere to the naming convention
    // and its keys must be the same names as the database columns.
    // ex. $dataArray['articleID'], $dataArray['articleTitle']
    function sanitize($dataArray)
    {
        // apply sanitize rules

        $dataArray['userName'] = filter_var($dataArray['userName'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);

        $dataArray['userPassword'] = filter_var($dataArray['userPassword'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);

        $dataArray['userLevel'] = filter_var($dataArray['userLevel'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
        
        return $dataArray;
    }

    function load($userID)
    {
        $isLoaded = false;
        
        // load from database, store to data property
        $loadSQL = "SELECT * FROM users WHERE userID = ?";
        
        $stmt = $this->db->prepare($loadSQL);
        
        if ($stmt)
        {
            if ($stmt->execute(array($userID)))
            {
                $this->set($stmt->fetch(PDO::FETCH_ASSOC));
                
                $isLoaded = true;
            }
        }

        return $isLoaded;
    }

    function save_image($fileData)
    {
        if (isset($fileData['userImage']))
        {
            move_uploaded_file($fileData['userImage']['tmp_name'], "images/" . $this->data['userID'] . "_user.jpg");
        }
    }
    
    function save()
    {
        $isSaved = false;
        // save the data inside of data property
        
        // if data is new, INSERT.  check $this->data['articleID'] to see if it has a value.
        if (empty($this->data['userID'])) // insert
        {
            $insertSQL = "INSERT INTO users 
                    (userName, userPassword, userLevel) 
                    VALUES 
                    (?, ?, ?)";
            
            $stmt = $this->db->prepare($insertSQL);

            if ($stmt)
            {
                $data = $this->data;
                unset($data['userID']);
                
                if ($stmt->execute(array_values($data)))
                {
                    $isSaved = true;
                }
            } 
            else
            {
                $this->errors[] = "Error saving, please contact: ";
            }                
        }
        else // if data is not new, UPDATE
        {
            $updateSQL = "UPDATE users SET 
                    userID = ?,
                    userName = ?,
                    userPassword = ?,
                    userLevel = ?
                WHERE userID = ?
            ";
            
            $stmt = $this->db->prepare($updateSQL);

            if ($stmt)
            {                
                // NOTE: execute required regular arrays vs associative array
                if ($stmt->execute(array_merge(array_values($this->data), array($this->data['userID']))))
                {
                    $isSaved = true;
                }
            }                        
            else
            {
                $this->errors[] = "Error saving, please contact: ";
            }    
        }
                
        return $isSaved;
    }

    // validate the $this->data array and store any errors in $this->errors
    function validate()
    {
        $isValid = true;
        
        // validate individual values in $this->data
        if (empty($this->data['userName']))
        {
            $this->errors[] = "User Name is required";
            $isValid = false;
        }
        else 
        {
            if (strlen($this->data['userName']) > 15)
            {
                $this->errors[] = "User Name must be less than 15 characters";
                $isValid = false;
            }            
        }

        if (empty($this->data['userPassword']))
        {
            $this->errors[] = "User Password is required";
            $isValid = false;
        }
        else 
        {
            if (strlen($this->data['userPassword']) > 15)
            {
                $this->errors[] = "User Password must be less than 15 characters";
                $isValid = false;
            }            
        }

        if (empty($this->data['userLevel']))
        {
            $this->errors[] = "User Level is required";
            $isValid = false;
        }
        else 
        {
            if (strlen($this->data['userLevel']) > 1)
            {
                $this->errors[] = "User Level must be 1 character only";
                $isValid = false;
            }            
        }
        
        return $isValid;
    }

    function getList()
    {
        $selectSQL = "SELECT * FROM users ORDER BY userID";
        
        $stmt = $this->db->query($selectSQL);
     
        $userList = $stmt->fetchAll(PDO::FETCH_ASSOC);
        
        return $userList;
    }

    function getUserLevels()
    {
        return array(
          "1" => "Site User",
          "2" => "News Editor",
          "3" => "Administrator",
          "4" => "Super Administrator"
        );
    }

// check this function below versus Garritt's from week 8 folder - changes needed

    function login($userName, $userPassword)
    {
        $validUser = false;
        $userName = trim($userName);
        $userPassword = trim($userPassword);
        
        $loginSQL = "SELECT userID FROM users WHERE userName = ? AND userPassword = ?";
        
        $stmt = $this->db->prepare($loginSQL);

        if ($stmt)
        {
            if ($stmt->execute(array($userName, $userPassword)))
            {
                $data = $stmt->fetch(PDO::FETCH_ASSOC);
                
                // ensuring we have an array from the fetch and that it has a user id > 0
                if ($data && $data['userID'] > 0)
                {                
                    $userID = $data['userID'];
                }
            }
        }

        // returning the user id so on calling code we can use false or a value > 0 as 
        // indicator of success
        return $userID;        
    }

    function checkLogin($userName, $userPassword)
    {
        $userID = false;
        
        // query for the user id based on username and password
        $checkSQL = "SELECT userID FROM users WHERE userName = ? AND userPassword = ?";
        
        $stmt = $this->db->prepare($checkSQL);
        
        if ($stmt)
        {
            if ($stmt->execute(array($userName, $userPassword)))
            {
                $data = $stmt->fetch(PDO::FETCH_ASSOC);
                
                // ensuring we have an array from the fetch and that it has a user id > 0
                if ($data && $data['userID'] > 0)
                {                
                    $userID = $data['userID'];
                }
            }
        }

        // returning the user id so on calling code we can use false or a value > 0 as 
        // indicator of success
        return $userID;        
    }

    function userHasAccess($userID, $userLevel)
    {
        // default to no access
        $userHasAccess = false;
                
        // first load the user and if successful check
        if ($this->load($userID))
        {
            // if the user loaded, see if they have the access level passed in
            if ($this->data['userLevel'] == $userLevel)
            {
                $userHasAccess = true;
            }
        }
                
        return $userHasAccess;
    }

    function runReport($filterArray, $pageNumber = 1, $perPage = 20)
    {
        $usersList = array();
        
        $reportSQL = "SELECT * FROM users WHERE userName LIKE ?
            LIMIT " . ((($pageNumber - 1) * $perPage)) . ", " . $perPage;
        
        $stmt = $this->db->prepare($reportSQL);
        
        if ($stmt)
        {
            if ($stmt->execute(array('%' . $filterArray['usersFilter'] . '%')))
            {
                $usersList = $stmt->fetchAll(PDO::FETCH_ASSOC);
            }
        }

        return $usersList;
    }

    // downloads a report in CSV format
    function downloadReport($filterArray)
    {
        // initialize an array to hold the data
        $usersList = array();
        
        // build the query for the report
        $reportSQL = "SELECT * FROM users WHERE userName LIKE ? ";
        
        // because it has a ? parameter we need to 
        // prepare it.  prevents SQL injection.
        $stmt = $this->db->prepare($reportSQL);
        
        if ($stmt)
        {
            // we use wildcard %s in the value we are
            // passing to execute to allow partial searching
            if ($stmt->execute(array('%' . $filterArray['usersFilter'] . '%')))
            {
                // if we found data, fetch it into an array
                $usersList = $stmt->fetchAll(PDO::FETCH_ASSOC);
            }
        }
        
        // headers allow use to push data to the browser
        // in a specific format.  in this case CSV.
        header('Content-Type: text/csv');
        // send the name of the file to the browser
        header('Content-Disposition: attachment; filename="users_report.csv"');
        
        // send the data to the browser
        foreach ($usersList as $userData)
        {
            // data needs to be in CSV format
            // need the carriage return/line feed to format
            // properly
            echo implode(",", $userData) . "\r\n";
        }        
    }
    
}
?>