<html>
    <body>
        <form action="<?php echo $_SERVER['SCRIPT_NAME']; ?>" method="GET">
            Author: <input type="text" name="authorFilter"/><br>
            <input type="submit" name="btnViewReport" value="View Report"/>
        </form>
        
        <a href="article_download.php?authorFilter=<?php echo $_GET['authorFilter']; ?>">Download</a><br>
        
        <?php if (count($articleList) > 0) // if we have articles to show
        { ?>
        <table border="1">
            <header>
                <tr>
                    <th>
                       Article Title
                    </th>
                    <th>
                       Article Author
                    </th>
                    <th>
                       Article Date
                    </th>
                </tr>
            </header>
            <?php foreach ($articleList as $currentArticleData) // loop through each article
            { ?>
            <tr>            
                <td>
                    <?php echo $currentArticleData['articleTitle']; ?>
                </td>
                <td>
                    <?php echo $currentArticleData['articleAuthor']; ?>
                </td>
                <td>
                    <?php echo $currentArticleData['articleDate']; ?>
                </td>
            </tr>
            <?php } ?>
        </table>
        <?php } ?>
        <div>
            <?php if ($pageNumber > 1) // if this is not page one, show previous
            { ?>
                <a href="<?php echo $_SERVER['SCRIPT_NAME']; ?>?pageNumber=<?php echo ($pageNumber - 1); ?>&authorFilter=<?php echo $_GET['authorFilter']; ?>">Previous</a>
            <?php } ?>
            <?php if ($pageNumber >= 1 && count($articleList) > 0) // if this is page one or greater, show next
            { ?>
                <a href="<?php echo $_SERVER['SCRIPT_NAME']; ?>?pageNumber=<?php echo ($pageNumber + 1); ?>&authorFilter=<?php echo $_GET['authorFilter']; ?>">Next</a>
            <?php } ?>
        </div>
    </body>    
</html>
