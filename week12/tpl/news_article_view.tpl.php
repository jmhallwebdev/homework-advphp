<html>
    
    Article Title: <?php echo (isset($articleData['articleTitle']) ? $articleData['articleTitle'] : "" ); ?><br>
    Article Content: 
    <?php echo (isset($articleData['articleContent']) ? $articleData['articleContent'] : "" ); ?><br>
    Article Author: <?php echo (isset($articleData['articleAuthor']) ? $articleData['articleAuthor'] : "" ); ?><br>
    Article Date: <?php echo (isset($articleData['articleDate']) ? $articleData['articleDate'] : "" ); ?><br>
    
    <?php if (file_exists("images/" . $articleData['articleID'] . "_" . $articleImageNameSuffix . ".jpg")) // if we have an image display it
    { ?>
        <img src="<?php echo "images/" . $articleData['articleID'] . "_" . $articleImageNameSuffix . ".jpg"; ?>"/>
    <?php } ?>
    
    <a href="news_article_list.php">Back to List</a>
</html>