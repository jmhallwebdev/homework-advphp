<html>
    <a href="news_article_edit.php">Add Article</a>
    <a href="news_report.php">Article Report</a>
    <table border="1">
        <header>
            <tr>
                <th>
                   Article Title
                </th>
                <th>
                   Article Author
                </th>
                <th>
                   Article Date
                </th>
                <th>
                   View
                </th>
                <th>
                   Edit
                </th>
            </tr>
        </header>
        <?php foreach ($newsList as $currentArticleData) 
        { ?>
        <tr>            
            <td>
                <?php echo $currentArticleData['articleTitle']; ?>
            </td>
            <td>
                <?php echo $currentArticleData['articleAuthor']; ?>
            </td>
            <td>
                <?php echo $currentArticleData['articleDate']; ?>
            </td>
            <td>
                <a href="news_article_view.php?articleID=<?php echo $currentArticleData['articleID']; ?>">View</a>
            </td>
            <td>
                <a href="news_article_edit.php?articleID=<?php echo $currentArticleData['articleID']; ?>">Edit</a>
            </td>
        </tr>
        <?php } ?>
    </table>
</html>
