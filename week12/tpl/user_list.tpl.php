<html>
    <a href="user_edit.php">Add User</a>
    <table border="1">
        <header>
            <tr>
                <th>
                   Username
                </th>
                <th>
                   User Level
                </th>
                <th>
                   View
                </th>
                <th>
                   Edit
                </th>
            </tr>
        </header>
        <?php foreach ($userList as $currentUserData) 
        { ?>
        <tr>            
            <td>
                <?php echo $currentUserData['username']; ?>
            </td>
            <td>
                <?php echo $userLevels[$currentUserData['user_level']]; ?>
            </td>
            <td>
                <a href="user_view.php?user_id=<?php echo $currentUserData['user_id']; ?>">View</a>
            </td>
            <td>
                <a href="user_edit.php?user_id=<?php echo $currentUserData['user_id']; ?>">Edit</a>
            </td>
        </tr>
        <?php } ?>
    </table>
</html>
