<?php
// need the session so we can check the users access
session_start();

require_once('../inc/newsArticles.class.php');
require_once('../inc/users.class.php');

// need instance of users to check access
$users = new users();

// if there is no user saved in session (ie no login) or the user 
// doesnt have access, kick back to list page.
// go to user_login page first.
if (!isset($_SESSION['user_id']) || !$users->userHasAccess($_SESSION['user_id'], "4"))
{
    header("location: news_article_list.php");
    exit;
}

// create an instance of the news article class
$newsArticle = new newsArticles();

// setup our paging, default 1 or or use what is in the GET parameter
if (isset($_GET['pageNumber']) && $_GET['pageNumber'] > 1)
{
    $pageNumber = $_GET['pageNumber'];
}
else
{
    $pageNumber = 1;
}

// default to an empty list
$articleList = array();

// check to see if button was click
if (isset($_GET['authorFilter']))
{
    // run report
    $articleList = $newsArticle->runReport($_GET, $pageNumber);
}


include('../tpl/news_report.tpl.php');
?>