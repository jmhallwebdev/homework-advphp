<?php
// the goal of this api page is to return all articles
// or a single article in JSON format

require_once('../inc/newsArticles.class.php');

// create an instance of the news article class
$newsArticle = new newsArticles();

// if we have an article id, then retrieve it
if (isset($_GET['articleID']) && $_GET['articleID'] > 0)
{
    // load the article
    if ($newsArticle->load($_GET['articleID']))
    {
        // if successful, show the article in JSON
        echo json_encode($newsArticle->data);
    }        
}
else
{
    // get the list of articles
    $articleList = $newsArticle->getList();
    // show in JSON
    echo json_encode($articleList);
}
?>