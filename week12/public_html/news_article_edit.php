<?php
require_once('../inc/newsArticles.class.php');

var_dump($_FILES);

// if we are attempting to save 
if (isset($_POST['btnCancel']))
{
    header("location:news_article_list.php");
    exit;
}

// create an instance of the news article class
$newsArticle = new newsArticles();

// check to see if we have an id to load, if so, load it
// this can be from GET or POST
if (isset($_REQUEST['articleID'])) 
{
    // load the article
    $newsArticle->load($_REQUEST['articleID']);
}

$errorMsg = '';

// if we are attempting to save 
if (isset($_POST['btnSave']))
{
    // we dont want the button anymroe
    unset($_POST['btnSave']);
    
    // sanitize the data from post and store into a temporary array
    $dataArray = $newsArticle->sanitize($_POST);
    
    // set our internal data array to the posted data
    $newsArticle->set($dataArray);
    
    // make sure the data is valid before we can save it
    if ($newsArticle->validate())
    {
        // attempt to save
        if ($newsArticle->save())
        {
            // if the article saved, now save the image
            $newsArticle->save_image($_FILES);
            
            // once we have saved, move to a success page
            // instead of staying on this page.  this prevents
            // double posting on refresh.
            header("location: news_article_saved.php");
            exit;
        }
        else
        {
            $errorMsg = "Failed to save";
        }
    }
}

// get the data the display will need.  we dont want to use the business 
// object instance directly.
$formData = $newsArticle->data;
$errors = $newsArticle->errors;

if (!empty($errorMsg))
{
    $errors[] = $errorMsg;
}

include('../tpl/news_article_edit.tpl.php');
?>