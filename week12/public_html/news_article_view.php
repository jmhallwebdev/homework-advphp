<?php
require_once('../inc/newsArticles.class.php');

// create an instance of the news article class
$newsArticle = new newsArticles();

// check to see if we have an id to load, if so, load it
// this can be from GET or POST
if (isset($_REQUEST['articleID'])) 
{
    // load the article
    $newsArticle->load($_REQUEST['articleID']);
}

// get the data the display will need.  we dont want to use the business 
// object instance directly.
$articleData = $newsArticle->data;
$articleImageNameSuffix = $newsArticle->imageFormFieldName;

include('../tpl/news_article_view.tpl.php');
?>