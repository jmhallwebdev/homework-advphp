<?php
require_once('../inc/db.config.php');
require_once('../inc/base.class.php');

class users extends base 
{    
    
    var $tableName = "users";
    var $keyField = "user_id";

    var $tableColumnNames = array(
        "user_id",
        "username", 
        "password", 
        "user_level"
    ); // array of column names in table
    
    
    var $imageFormFieldName = "userImage";
    
    // override to allow performing special code unique to users class
    function __construct() 
    {
        // i still want to call the parent though to get the parent code to run
        parent::__construct();

        // do my special code for users
        
    }    

    // validate the $this->data array and store any errors in $this->errors
    function validate()
    {
        $isValid = parent::validate();
        
        // validate individual values in $this->data
        if (empty($this->data['username']))
        {
            $this->errors[] = "Username is required";
            $isValid = false;
        }
        else 
        {
            if (strlen($this->data['username']) > 120)
            {
                $this->errors[] = "Username must be less than 120 characters";
                $isValid = false;
            }            
        }
        
        return $isValid;
    }
    
    function getList()
    {
        $selectSQL = "SELECT * FROM users ORDER BY username, user_level";
        
        $stmt = $this->db->query($selectSQL);
     
        $userList = $stmt->fetchAll(PDO::FETCH_ASSOC);
        
        return $userList;
    }
    
    // returns a common array of user levels that can be reused in other areas.
    // changing or adding here updates across the system.
    function getUserLevels()
    {
        return array(
          "1" => "Site User",
          "2" => "News Editor",
          "3" => "Administrator",
          "4" => "Super Administrator"
        );
    }
    
    function checkLogin($username, $password)
    {
        $user_id = false;
        
        // query for the user id based on username and password
        $checkSQL = "SELECT user_id FROM users WHERE username = ? AND password = ?";
        
        $stmt = $this->db->prepare($checkSQL);
        
        if ($stmt)
        {
            if ($stmt->execute(array($username, $password)))
            {
                $data = $stmt->fetch(PDO::FETCH_ASSOC);
                
                // ensuring we have an array from the fetch and that it has a user id > 0
                if ($data && $data['user_id'] > 0)
                {                
                    $user_id = $data['user_id'];
                }
            }
        }

        // returning the user id so on calling code we can use false or a value > 0 as 
        // indicator of success
        return $user_id;        
    }
    
    // function to return if a user has the access level passed in
    function userHasAccess($userID, $userLevel)
    {
        // default to no access
        $userHasAccess = false;
                
        // first load the user and if successful check
        if ($this->load($userID))
        {
            // if the user loaded, see if they have the access level passed in
            if ($this->data['user_level'] == $userLevel)
            {
                $userHasAccess = true;
            }
        }
                
        return $userHasAccess;
    }
    
    
}



?>