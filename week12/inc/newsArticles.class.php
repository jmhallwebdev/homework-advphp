<?php
require_once('../inc/db.config.php');
require_once('../inc/base.class.php');

class newsArticles extends base 
{
    
    var $tableName = "newsarticles";
    var $keyField = "articleID";
    var $imageFormFieldName = "articleImage";
    var $tableColumnNames = array(
        "articleID",
        "articleTitle", 
        "articleContent", 
        "articleAuthor", 
        "articleDate"            
    ); // array of column names in table

    // data array can come from anywhere, but it has to adhere to the naming convention
    // and its keys must be the same names as the database columns.
    // ex. $dataArray['articleID'], $dataArray['articleTitle']
    function sanitize($dataArray)
    {
        $dataArray = parent::sanitize($dataArray);

        // apply news articles pecific sanitize rules
        
        return $dataArray;
    }

    // validate the $this->data array and store any errors in $this->errors
    function validate()
    {
        $isValid = parent::validate();
        
        // validate individual values in $this->data
        if (empty($this->data['articleTitle']))
        {
            $this->errors[] = "Article Title is required";
            $isValid = false;
        }
        else 
        {
            if (strlen($this->data['articleTitle']) > 150)
            {
                $this->errors[] = "Article Title must be less than 150 characters";
                $isValid = false;
            }            
        }
        
        return $isValid;
    }
    
    function getList()
    {
        $selectSQL = "SELECT * FROM newsarticles ORDER BY articleAuthor";
        
        $stmt = $this->db->query($selectSQL);
     
        $articleList = $stmt->fetchAll(PDO::FETCH_ASSOC);
        
        return $articleList;
    }
    
    // exports articles into the file passed as CSV
    function exportArticles($filename)
    {
        $articleList = $this->getList();
        
        // did we get an array and does it have data
        if (is_array($articleList) && count($articleList) > 0)
        {                        
            // open the file for writing, creates or truncates
            $export_file = fopen($filename, "w");
            
            // check to make sure handle is good
            if ($export_file) 
            {
                // export header
                fputcsv($export_file, array_keys($articleList[0]));
                
                // for each article, write to file
                foreach ($articleList as $articleData)
                {
                    fputcsv($export_file, $articleData);
                }
                
                // close up the file
                fclose($export_file);
            }
            
        }                
    }
    
    // imports data from an external file
    function importArticles($filename)
    {
        if (file_exists($filename))
        {
            // open the file for reading
            $import_file = fopen($filename, "r");
            
            $lineCnt = 0;
            $headerArray = array(); // place to store the header
            
            while ($import_file && !feof($import_file))
            {               
                // get a row of data into an array
                $rowData = fgetcsv($import_file);
                
                if ($lineCnt++ > 0) // if this is not the first row
                {
                    // merge the header with the data so we can use further
                    $rowData = array_combine($headerArray, $rowData);
                    
                    // call set so we can use validate/save
                    $this->set($rowData);
                    
                    //var_dump($this->data);
                    
                    if ($this->validate()) 
                    {
                        $this->save();
                    }
                    
                }
                else // we have a header
                {
                     $headerArray = $rowData;
                }
            }
            
            // close up the file
            fclose($import_file);
        }
    }
    
    function runReport($filterArray, $pageNumber = 1, $perPage = 20)
    {
        $articleList = array();
        
        $reportSQL = "SELECT * FROM " . 
            $this->tableName . 
            " WHERE articleAuthor LIKE ?
            LIMIT " . ((($pageNumber - 1) * $perPage)) . ", " . $perPage;
        
        $stmt = $this->db->prepare($reportSQL);
        
        if ($stmt)
        {
            if ($stmt->execute(array('%' . $filterArray['authorFilter'] . '%')))
            {
                $articleList = $stmt->fetchAll(PDO::FETCH_ASSOC);
            }
        }

        return $articleList;
    }

    // downloads a report in CSV format
    function downloadReport($filterArray)
    {
        // initialize an array to hold the data
        $articleList = array();
        
        // build the query for the report
        $reportSQL = "SELECT * FROM " . 
            $this->tableName . 
            " WHERE articleAuthor LIKE ? ";
        
        // because it has a ? parameter we need to 
        // prepare it.  prevents SQL injection.
        $stmt = $this->db->prepare($reportSQL);
        
        if ($stmt)
        {
            // we use wildcard %s in the value we are
            // passing to execute to allow partial searching
            if ($stmt->execute(array('%' . $filterArray['authorFilter'] . '%')))
            {
                // if we found data, fetch it into an array
                $articleList = $stmt->fetchAll(PDO::FETCH_ASSOC);
            }
        }
        
        // headers allow use to push data to the browser
        // in a specific format.  in this case CSV.
        header('Content-Type: text/csv');
        // send the name of the file to the browser
        header('Content-Disposition: attachment; filename="article_report.csv"');
        
        // send the data to the browser
        foreach ($articleList as $articleData)
        {
            // data needs to be in CSV format
            // need the carriage return/line feed to format
            // properly
            echo implode(",", $articleData) . "\r\n";
        }        
    }    
}
?>