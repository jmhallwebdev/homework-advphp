<?php

class base {
    
    var $data = array(); // stores the class data 
    var $errors = array(); // stores error information
    var $db = null; // stores a pdo connection

    var $tableName = "";
    var $keyField = "";
    var $tableColumnNames = array(); // array of column names in table
    
    var $imageFormFieldName = "";
    
    function __construct() 
    {
        $this->connectToDB();
    }
    
    function connectToDB()
    {
        $isConnected = true;
        
        // constructor of PDO always throws an exception
        try {
            $this->db = new PDO(DATABASE_CONNECTION_STRING, DATABASE_USER, DATABASE_PASS, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING));
        } 
        catch (PDOException $e)
        {
            $this->errors[] = 'Failed to connect to database';
            $this->errors[] = $e->getMessage();
            $isConnected = false;
        } 
        
        return $isConnected;
    }    
    
    // data array can come from anywhere, but it has to adhere to the naming convention
    // and its keys must be the same names as the database columns.
    // ex. $dataArray['articleID'], $dataArray['articleTitle']
    function set($dataArray)
    {
        $this->data = $dataArray;
    }    
        
    // data array can come from anywhere, but it has to adhere to the naming convention
    // and its keys must be the same names as the database columns.
    // ex. $dataArray['articleID'], $dataArray['articleTitle']
    function sanitize($dataArray)
    {
        // apply sanitize rules
        
        return $dataArray;
    }    

    function validate()
    {
        return true;
    }
    
    function load($id)
    {
        $isLoaded = false;
        
        // load from database, store to data property
        $loadSQL = "SELECT * FROM " . 
            $this->tableName . " WHERE " . 
            $this->keyField . " = ?";
        
        $stmt = $this->db->prepare($loadSQL);
        
        if ($stmt)
        {
            if ($stmt->execute(array($id)))
            {
                $this->set($stmt->fetch(PDO::FETCH_ASSOC));
                
                $isLoaded = true;
            }
        }

        return $isLoaded;
    }

    function save_image($fileData)
    {
        if (isset($fileData[$this->imageFormFieldName]))
        {
            move_uploaded_file($fileData[$this->imageFormFieldName]['tmp_name'], "images/" . $this->data[$this->keyField] . "_" . $this->imageFormFieldName . ".jpg");
        }
    }    
    
    function save()
    {
        $isSaved = false;
        // save the data inside of data property
        
        // if data is new, INSERT.  check $this->data['user_id'] to see if it has a value.
        if (empty($this->data[$this->keyField])) // insert
        {
            
            $insertSQL = "INSERT INTO " . $this->tableName . " ";
            
            $columnCnt = 0;
            
            foreach ($this->tableColumnNames as $columnName) 
            {
                if ($columnName != $this->keyField)
                {
                    if ($columnCnt == 0) 
                    {
                        $insertSQL .= "(";
                    }
                    else 
                    {
                        $insertSQL .= ", ";
                    }
                                        
                    $insertSQL .= $columnName;
                    
                    $columnCnt++;                    
                }                
            }
            
            $insertSQL .= ")";
            
            $insertSQL .= " VALUES (";
            
            for ($i = 0; $i < $columnCnt; $i++)
            {
                if ($i > 0)
                {
                    $insertSQL .= ", ?";
                }
                else 
                {       
                    $insertSQL .= "?";
                }
            }
            
            $insertSQL .= ")";
                                    
            $stmt = $this->db->prepare($insertSQL);

            if ($stmt)
            {
                $data = $this->data;
                unset($data[$this->keyField]);
                                
                if ($stmt->execute(array_values($data)))
                {
                    // store the id back so we can have it beyond this point
                    $this->data[$this->keyField] = $this->db->lastInsertId();
                    $isSaved = true;
                }
            } 
            else
            {
                $this->errors[] = "Error saving, please contact: ";
            }                
        }
        else // if data is not new, UPDATE
        {
            $updateSQL = "UPDATE " . $this->tableName . " SET ";
            
            $columnCnt = 0;
            
            foreach ($this->tableColumnNames as $columnName) 
            {
                if ($columnCnt > 0)
                {
                    $updateSQL .= ", ";
                }
                
                $updateSQL .= $columnName . " = ?";
                
                $columnCnt++;
            }
            
            $updateSQL .= " WHERE " . $this->keyField . " = ?";

            $stmt = $this->db->prepare($updateSQL);

            if ($stmt)
            {                
                // NOTE: execute required regular arrays vs associative array
                if ($stmt->execute(array_merge(array_values($this->data), array($this->data[$this->keyField]))))
                {
                    $isSaved = true;
                }
            }                        
            else
            {
                $this->errors[] = "Error saving, please contact: ";
            }    
        }
                
        return $isSaved;
    }
    
}
?>