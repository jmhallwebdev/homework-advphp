<?php
require_once('../inc/db.config.php');

class newsArticles 
{
    
    var $data = array(); // stores the article data 
    var $errors = array(); // stores error information
    var $db = null; // stores a pdo connection
    
    function __construct() 
    {
        $this->connectToDB();
    }
    
    function connectToDB()
    {
        $isConnected = true;
        
        // constructor of PDO always throws an exception
        try {
            $this->db = new PDO(DATABASE_CONNECTION_STRING, DATABASE_USER, DATABASE_PASS, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING));
        } 
        catch (PDOException $e)
        {
            $this->errors[] = 'Failed to connect to database';
            $this->errors[] = $e->getMessage();
            $isConnected = false;
        } 
        
        return $isConnected;
    }
    
    // data array can come from anywhere, but it has to adhere to the naming convention
    // and its keys must be the same names as the database columns.
    // ex. $dataArray['articleID'], $dataArray['articleTitle']
    function set($dataArray)
    {
        $this->data = $dataArray;
    }
    
    // data array can come from anywhere, but it has to adhere to the naming convention
    // and its keys must be the same names as the database columns.
    // ex. $dataArray['articleID'], $dataArray['articleTitle']
    function sanitize($dataArray)
    {

        $dataArray['articleTitle'] = filter_var($dataArray['articleTitle'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);

        $dataArray['articleContent'] = filter_var($dataArray['articleContent'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);

        $dataArray['articleAuthor'] = filter_var($dataArray['articleAuthor'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);

        $dataArray['articleDate'] = filter_var($dataArray['articleDate'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
        
        return $dataArray;
    }

    function load($articleID)
    {
        $isLoaded = false;
        
        // load from database, store to data property
        $loadSQL = "SELECT * FROM newsarticles WHERE articleID = ?";
        
        $stmt = $this->db->prepare($loadSQL);
        
        if ($stmt)
        {
            if ($stmt->execute(array($articleID)))
            {
                $this->set($stmt->fetch(PDO::FETCH_ASSOC));
                
                $isLoaded = true;
            }
        }

        return $isLoaded;
    }
    
    function save()
    {
        $isSaved = false;
        // save the data inside of data property
        
        // if data is new, INSERT.  check $this->data['articleID'] to see if it has a value.
        if (empty($this->data['articleID'])) // insert
        {
            $insertSQL = "INSERT INTO newsarticles 
                    (articleTitle, articleContent, articleAuthor, articleDate) 
                    VALUES 
                    (?, ?, ?, ?)";
            
            $stmt = $this->db->prepare($insertSQL);

            if ($stmt)
            {
                $data = $this->data;
                unset($data['articleID']);
                
                if ($stmt->execute(array_values($data)))
                {
                    $isSaved = true;
                }
            }            
        }
        else // if data is not new, UPDATE
        {
            $updateSQL = "UPDATE newsarticles SET 
                    articleID = ?,
                    articleTitle = ?,
                    articleContent = ?,
                    articleAuthor = ?,
                    articleDate = ?
                WHERE articleID = ?
            ";
            
            $stmt = $this->db->prepare($updateSQL);

            if ($stmt)
            {                
                // NOTE: execute required regular arrays vs associative array
                if ($stmt->execute(array_merge(array_values($this->data), array($this->data['articleID']))))
                {
                    $isSaved = true;
                }
            }                        
        }
                
        return $isSaved;
    }

    // validate the $this->data array and store any errors in $this->errors
    function validate()
    {
        $isValid = true;
        
        // validate individual values in $this->data
        if (empty($this->data['articleTitle']))
        {
            $this->errors[] = "Article Title is required";
            $isValid = false;
        }

        else
        {
            $dataArray['articleTitle'] = htmlspecialchars($dataArray['articleTitle']);
            $dataArray['articleTitle'] = trim($dataArray['articleTitle']);
            $dataArray['articleTitle'] = stripslashes($dataArray['articleTitle']);
        }
        
        if (empty($this->data['articleContent']))
        {
            $this->errors[] = "Article Content is required";
            $isValid = false;
        }

        else
        {
            $dataArray['articleContent'] = htmlspecialchars($dataArray['articleContent']);
            $dataArray['articleContent'] = trim($dataArray['articleContent']);
            $dataArray['articleContent'] = stripslashes($dataArray['articleContent']);
        }
        
        if (empty($this->data['articleAuthor']))
        {
            $this->errors[] = "Article Author is required";
            $isValid = false;
        }

        else
        {
            $dataArray['articleAuthor'] = htmlspecialchars($dataArray['articleAuthor']);
            $dataArray['articleAuthor'] = trim($dataArray['articleAuthor']);
            $dataArray['articleAuthor'] = stripslashes($dataArray['articleAuthor']);
        }
        
        if (empty($this->data['articleDate']))
        {
            $this->errors[] = "Article Date is required";
            $isValid = false;
        }

        else
        {
            $dataArray['articleDate'] = htmlspecialchars($dataArray['articleDate']);
            $dataArray['articleDate'] = trim($dataArray['articleDate']);
            $dataArray['articleDate'] = stripslashes($dataArray['articleDate']); 

            $myDateRegex = "^(((((0[13578])|([13578])|(1[02]))[\-\/\s]?((0[1-9])|([1-9])|([1-2][0-9])|(3[01])))|((([469])|(11))[\-\/\s]?((0[1-9])|([1-9])|([1-2][0-9])|(30)))|((02|2)[\-\/\s]?((0[1-9])|([1-9])|([1-2][0-9]))))[\-\/\s]?\d{4})(\s(((0[1-9])|([1-9])|(1[0-2]))\:([0-5][0-9])((\s)|(\:([0-5][0-9])\s))([AM|PM|am|pm]{2,2})))?$";

            //http://regexlib.com/REDetails.aspx?regexp_id=361 (the above regex pattern)
            
            if (!preg_match($myDateRegex, $dataArray['articleDate'])) 
            {
                $isValid = false;
            } 

        }
        
        return $isValid;
    }
    
    function getList()
    {
        $selectSQL = "SELECT * FROM newsarticles ORDER BY articleAuthor";
        
        $stmt = $this->db->query($selectSQL);
     
        $articleList = $stmt->fetchAll(PDO::FETCH_ASSOC);
        
        return $articleList;
    }
}



?>