<?php
require_once(dirname(__FILE__) . "/BaseClass.class.php");

class users extends BaseClass {
    
    // stubbed functions potentially needed for users
    public function isUserValid();
    public function isUserAnAdmin();
    
    // specific function using generic function to load data
    public function loadUser($userId)
    {
        $this->loadData("user", "user_id", $userId);
    }
        
}
?>