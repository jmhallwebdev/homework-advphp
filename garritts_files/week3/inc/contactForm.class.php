<?php
class ContactForm 
{
    
    // function meant to validate the input from the contact form
    // this takes two parameters, an array of information that was entered and
    // an array to return error messages into.  the use of the & allows us to 
    // add values to the errors array for use in the calling functionality
    public function validate($inputArray, &$errorsArray)
    {
        $success = true;
        
        if (empty($inputArray['first_name']))
        {
            $errorsArray[] = "Please enter a first name";
            $success = false;
        }
                
        if (filter_var($inputArray['email_address'], FILTER_VALIDATE_EMAIL) === false)
        {
            $errorsArray[] = "Please enter a valid email address";
            $success = false;            
        }
                
        return $success;
    }
    
    // function meant to sanitize the inputs from the form
    public function sanitize(&$inputArray)
    {
        $inputArray['email_address'] = filter_var($inputArray['email_address'], FILTER_SANITIZE_EMAIL);
    }
    
    // send the email using the info from the form.
    public function sendEmail($inputArray)
    {
        $emailMessage = "";
        
        foreach ($inputArray as $fieldName => $fieldValue)
        {
            $emailMessage .= $fieldName . ": " . $fieldValue . PHP_EOL;
            var_dump("1");            
        } 
        
        $emailMessage .= PHP_EOL . PHP_EOL . "Thank you!";
        
            var_dump("2");            
        $success = mail('gbgrandberg@dmacc.edu', "contact form", $emailMessage);
        
        var_dump($success); 
        
        debug_print_backtrace();
        
        die;
    }
}

?>