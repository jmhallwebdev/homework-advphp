<?php
require_once(dirname(__FILE__) . "/BaseClass.class.php");

// news specific class that utilizes the generic base to load data
class NewsArticle extends BaseClass
{
    public function loadArticle($articleId)
    {
        $this->loadData("articles", "article_id", $articleId);
    }
    
}
?>