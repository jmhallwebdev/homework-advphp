
<?php
// require the file so we can access the contact form class
require_once(dirname(__FILE__) . "/../inc/contactForm.class.php");

// instantiate an instance of the contact form class so we can call the 
// functionality
$contactForm = new ContactForm();

// store the post information for use by the rest of the script
$postInformation = $_POST;
// create an array to hold error information from the validation
$errorsArray = array();

// check if we are from a submit by the user
if (isset($_POST['submit']))
{
    // first sanitize the data because we want to validate sanitized
    $contactForm->sanitize($postInformation);

    // validate the sanitized data and store any errors into errorsArray
    $success = $contactForm->validate($postInformation, $errorsArray);

    // if the validation was successful, send the email and go to the thank you page
    if ($success) 
    {
        $contactForm->sendEmail($postInformation);
        header("location: contact_thankyou.php");
        exit;
    }
    
    // debug to show success and error information
    var_dump($success, $errorsArray);    
}

?>
<html>
    <body>
        <ul>
            <?php foreach ($errorsArray as $errorMsg) // looping the error messages if there are any
            { ?>
                <li><?php echo $errorMsg; ?></li>
            <?php }?>
        </ul>
        
        <form action="<?php echo $_SERVER['SCRIPT_NAME']; ?>" method="post">
            first name: 
            <input type="text" name="first_name" value="<?php echo (isset($postInformation['first_name']) ? $postInformation['first_name'] : ""); ?>"/><br>
            last name: <input type="text" name="last_name" value="<?php echo (isset($postInformation['last_name']) ? $postInformation['last_name'] : ""); ?>"/><br>
            dob (dd/mm/yyyy): <input type="text" name="dob" value="<?php echo (isset($postInformation['dob']) ? $postInformation['dob'] : ""); ?>"/><br>
            email address: <input type="text" name="email_address" value="<?php echo (isset($postInformation['email_address']) ? $postInformation['email_address'] : ""); ?>"/><br>
            message: <textarea name="message"><?php echo (isset($postInformation['message']) ? $postInformation['message'] : ""); ?></textarea><br>
            <input type="submit" name="submit" value="Contact"/>
        </form>
    </body>        
</html>