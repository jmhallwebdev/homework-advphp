<?php
    require_once("../inc/functions.inc.php");

    echo "hello";
    $userCanAccess = canUserAccess(1);
    
?>
<html>
    <body>
        <?php if ($userCanAccess)
        { ?>
            <div>user has access</div>
        <?php } else             
        { ?>
            <div>user does not have access</div>
        <?php } ?>
    </body>
</html>