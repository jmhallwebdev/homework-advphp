<?php

//takes the $POST variable and assigns it to new variables on the server side

$firstName = $_POST['firstName'];
$lastName = $_POST['lastName'];
$dob = $_POST['dob'];
$emailAddress = $_POST['emailAddress'];
$message = $_POST['message'];

$emailErrors = no;


// Remove all illegal characters from fields

$firstName = filter_var($firstName, FILTER_SANITIZE_STRING);

$lastName = filter_var($lastName, FILTER_SANITIZE_STRING);

$dob = filter_var($dob, FILTER_SANITIZE_STRING);

$emailAddress = filter_var($emailAddress, FILTER_SANITIZE_EMAIL);

$message = filter_var($message, FILTER_SANITIZE_STRING);



// html REQUIRED used for firstName & last name. No validation used here. 

// Validate e-mail

if (!filter_var($emailAddress, FILTER_VALIDATE_EMAIL) === false) {
    echo("Thank you. The email has been sent.");
} else {
	$emailErrors = yes;
    echo("$emailAddress is not a valid email address - use the browsers back button and please correct it.");
}


//builds the email, concatenation used to create the message. 

$to      = 'gbgrandberg@dmacc.edu';
$subject = 'Thank you for contacting us.';
$emailMessage = $firstName . ' ' . $lastName . ' ' . $dob . ' ' . $emailAddress . ' ' . $message;


//sends the email

if ($emailErrors === no) {
	mail($to, $subject, $emailMessage);
} else {
}
?>