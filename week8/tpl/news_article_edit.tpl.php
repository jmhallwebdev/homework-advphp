<html>
    <div>
        <span class=""><?php echo (isset($formData['articleID']) && $formData['articleID'] > 0 ? "Edit" : "Add" ); ?></span> News Article
    </div>
    
    <ul>
        <?php foreach ($errors as $errorMsg) 
        { ?>
        <li><?php echo $errorMsg; ?></li>
        <?php } ?>        
    </ul>
    
    <form method="post" action="<?php echo $_SERVER['SCRIPT_NAME']; ?>" enctype="multipart/form-data">
        <input type="hidden" name="articleID" value="<?php echo (isset($formData['articleID']) ? $formData['articleID'] : "" ); ?>"/><br>
        Article Title: <input type="text" name="articleTitle" value="<?php echo (isset($formData['articleTitle']) ? $formData['articleTitle'] : "" ); ?>"/><br>
        Article Content: 
        <textarea name="articleContent"><?php echo (isset($formData['articleContent']) ? $formData['articleContent'] : "" ); ?></textarea><br>
        Article Author: <input type="text" name="articleAuthor" value="<?php echo (isset($formData['articleAuthor']) ? $formData['articleAuthor'] : "" ); ?>"/><br>
        Article Date: <input type="text" name="articleDate" value="<?php echo (isset($formData['articleDate']) ? $formData['articleDate'] : "" ); ?>"/><br>
        Article Image: <input type="file" name="articleImage"/><br>
        <input type="submit" value="Save" name="btnSave"/>
        <input type="submit" value="Cancel" name="btnCancel"/>
    </form>    
</html>