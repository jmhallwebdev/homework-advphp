<html>
    <div>
        <span class=""><?php echo (isset($formData['user_id']) && $formData['user_id'] > 0 ? "Edit" : "Add" ); ?></span> News Article
    </div>
    
    <ul>
        <?php foreach ($errors as $errorMsg) 
        { ?>
        <li><?php echo $errorMsg; ?></li>
        <?php } ?>        
    </ul>
    
    <form method="post" action="<?php echo $_SERVER['SCRIPT_NAME']; ?>">
        <input type="hidden" name="user_id" value="<?php echo (isset($formData['user_id']) ? $formData['user_id'] : "" ); ?>"/><br>
        Username: <input type="text" name="username" value="<?php echo (isset($formData['username']) ? $formData['username'] : "" ); ?>"/><br>
        Password: <input type="password" name="password" value="<?php echo (isset($formData['password']) ? $formData['password'] : "" ); ?>"/><br>
        User Level: 
        <select name="user_level">
            <?php foreach ($userLevels as $userLevel => $levelDescription) 
            { ?>
                <option value="<?php echo $userLevel; ?>" <?php echo (isset($formData['user_level']) && $formData['user_level'] == $userLevel ? "selected" : "" ); ?>><?php echo $levelDescription; ?></option>
            <?php } ?>
        </select>
        
        <input type="submit" value="Save" name="btnSave"/>
        <input type="submit" value="Cancel" name="btnCancel"/>
    </form>    
</html>