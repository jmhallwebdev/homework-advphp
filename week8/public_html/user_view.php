<?php
require_once('../inc/users.class.php');

// create an instance of the news article class
$user = new users();

// check to see if we have an id to load, if so, load it
// this can be from GET or POST
if (isset($_REQUEST['user_id'])) 
{
    // load the article
    $user->load($_REQUEST['user_id']);
}

// get the data the display will need.  we dont want to use the business 
// object instance directly.
$userData = $user->data;
$userLevels = $user->getUserLevels();

include('../tpl/user_view.tpl.php');
?>