<?php
require_once('../inc/users.class.php');

// if we are attempting to save 
if (isset($_POST['btnCancel']))
{
    header("location:user_list.php");
    exit;
}

// create an instance of the user article class
$user = new users();

// check to see if we have an id to load, if so, load it
// this can be from GET or POST
if (isset($_REQUEST['user_id'])) 
{
    // load the article
    $user->load($_REQUEST['user_id']);
}

$errorMsg = '';

// if we are attempting to save 
if (isset($_POST['btnSave']))
{
    // we dont want the button anymroe
    unset($_POST['btnSave']);
    
    // sanitize the data from post and store into a temporary array
    $dataArray = $user->sanitize($_POST);
    
    // set our internal data array to the posted data
    $user->set($dataArray);
    
    // make sure the data is valid before we can save it
    if ($user->validate())
    {
        // attempt to save
        if ($user->save())
        {
            // once we have saved, move to a success page
            // instead of staying on this page.  this prevents
            // double posting on refresh.
            header("location: user_saved.php");
            exit;
        }
        else
        {
            $errorMsg = "Failed to save";
        }
    }
}

// get the data the display will need.  we dont want to use the business 
// object instance directly.
$formData = $user->data;
$errors = $user->errors;
$userLevels = $user->getUserLevels();

if (!empty($errorMsg)) 
{
    $errors[] = $errorMsg;
}


include('../tpl/user_edit.tpl.php');
?>