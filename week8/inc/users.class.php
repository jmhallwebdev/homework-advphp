<?php
require_once('../inc/db.config.php');

class users
{
    var $data = array(); // stores the article data 
    var $errors = array(); // stores error information
    var $db = null; // stores a pdo connection
    
    function __construct() 
    {
        $this->connectToDB();
    }
    
    function connectToDB()
    {
        $isConnected = true;
        
        // constructor of PDO always throws an exception
        try {
            $this->db = new PDO(DATABASE_CONNECTION_STRING, DATABASE_USER, DATABASE_PASS, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING));
        } 
        catch (PDOException $e)
        {
            $this->errors[] = 'Failed to connect to database';
            $this->errors[] = $e->getMessage();
            $isConnected = false;
        } 
        
        return $isConnected;
    }
    
    // data array can come from anywhere, but it has to adhere to the naming convention
    // and its keys must be the same names as the database columns.
    // ex. $dataArray['articleID'], $dataArray['articleTitle']
    function set($dataArray)
    {
        $this->data = $dataArray;
    }
    
    // data array can come from anywhere, but it has to adhere to the naming convention
    // and its keys must be the same names as the database columns.
    // ex. $dataArray['articleID'], $dataArray['articleTitle']
    function sanitize($dataArray)
    {
        // apply sanitize rules
        
        return $dataArray;
    }

    function load($userID)
    {
        $isLoaded = false;
        
        // load from database, store to data property
        $loadSQL = "SELECT * FROM users WHERE user_id = ?";
        
        $stmt = $this->db->prepare($loadSQL);
        
        if ($stmt)
        {
            if ($stmt->execute(array($userID)))
            {
                $this->set($stmt->fetch(PDO::FETCH_ASSOC));
                
                $isLoaded = true;
            }
        }

        return $isLoaded;
    }
    
    function save()
    {
        $isSaved = false;
        // save the data inside of data property
        
        // if data is new, INSERT.  check $this->data['user_id'] to see if it has a value.
        if (empty($this->data['user_id'])) // insert
        {
            $insertSQL = "INSERT INTO users  
                    (username, password, user_level) 
                    VALUES 
                    (?, ?, ?)";
            
            $stmt = $this->db->prepare($insertSQL);

            if ($stmt)
            {
                $data = $this->data;
                unset($data['user_id']);
                                
                if ($stmt->execute(array_values($data)))
                {
                    // store the id back so we can have it beyond this point
                    $this->data['user_id'] = $this->db->lastInsertId();
                    $isSaved = true;
                }
            } 
            else
            {
                $this->errors[] = "Error saving, please contact: ";
            }                
        }
        else // if data is not new, UPDATE
        {
            $updateSQL = "UPDATE users SET 
                    user_id = ?,
                    username = ?,
                    password = ?,
                    user_level = ? 
                WHERE user_id = ?
            ";
            
            $stmt = $this->db->prepare($updateSQL);

            if ($stmt)
            {                
                // NOTE: execute required regular arrays vs associative array
                if ($stmt->execute(array_merge(array_values($this->data), array($this->data['user_id']))))
                {
                    $isSaved = true;
                }
            }                        
            else
            {
                $this->errors[] = "Error saving, please contact: ";
            }    
        }
                
        return $isSaved;
    }

    // validate the $this->data array and store any errors in $this->errors
    function validate()
    {
        $isValid = true;
        
        // validate individual values in $this->data
        if (empty($this->data['username']))
        {
            $this->errors[] = "Username is required";
            $isValid = false;
        }
        else 
        {
            if (strlen($this->data['username']) > 120)
            {
                $this->errors[] = "Username must be less than 120 characters";
                $isValid = false;
            }            
        }
        
        return $isValid;
    }
    
    function getList()
    {
        $selectSQL = "SELECT * FROM users ORDER BY username, user_level";
        
        $stmt = $this->db->query($selectSQL);
     
        $userList = $stmt->fetchAll(PDO::FETCH_ASSOC);
        
        return $userList;
    }
    
    // returns a common array of user levels that can be reused in other areas.
    // changing or adding here updates across the system.
    function getUserLevels()
    {
        return array(
          "1" => "Site User",
          "2" => "News Editor",
          "3" => "Administrator",
          "4" => "Super Administrator"
        );
    }
    
    function checkLogin($username, $password)
    {
        $user_id = false;
        
        // query for the user id based on username and password
        $checkSQL = "SELECT user_id FROM users WHERE username = ? AND password = ?";
        
        $stmt = $this->db->prepare($checkSQL);
        
        if ($stmt)
        {
            if ($stmt->execute(array($username, $password)))
            {
                $data = $stmt->fetch(PDO::FETCH_ASSOC);
                
                // ensuring we have an array from the fetch and that it has a user id > 0
                if ($data && $data['user_id'] > 0)
                {                
                    $user_id = $data['user_id'];
                }
            }
        }

        // returning the user id so on calling code we can use false or a value > 0 as 
        // indicator of success
        return $user_id;        
    }
    
}



?>