<?php
require_once('../inc/db.config.php');

class newsArticles 
{
    
    var $data = array(); // stores the article data 
    var $errors = array(); // stores error information
    var $db = null; // stores a pdo connection
    
    function __construct() 
    {
        $this->connectToDB();
    }
    
    function connectToDB()
    {
        $isConnected = true;
        
        // constructor of PDO always throws an exception
        try {
            $this->db = new PDO(DATABASE_CONNECTION_STRING, DATABASE_USER, DATABASE_PASS, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING));
        } 
        catch (PDOException $e)
        {
            $this->errors[] = 'Failed to connect to database';
            $this->errors[] = $e->getMessage();
            $isConnected = false;
        } 
        
        return $isConnected;
    }
    
    // data array can come from anywhere, but it has to adhere to the naming convention
    // and its keys must be the same names as the database columns.
    // ex. $dataArray['articleID'], $dataArray['articleTitle']
    function set($dataArray)
    {
        $this->data = $dataArray;
    }
    
    // data array can come from anywhere, but it has to adhere to the naming convention
    // and its keys must be the same names as the database columns.
    // ex. $dataArray['articleID'], $dataArray['articleTitle']
    function sanitize($dataArray)
    {
        // apply sanitize rules
        
        return $dataArray;
    }

    function load($articleID)
    {
        $isLoaded = false;
        
        // load from database, store to data property
        $loadSQL = "SELECT * FROM newsarticles WHERE articleID = ?";
        
        $stmt = $this->db->prepare($loadSQL);
        
        if ($stmt)
        {
            if ($stmt->execute(array($articleID)))
            {
                $this->set($stmt->fetch(PDO::FETCH_ASSOC));
                
                $isLoaded = true;
            }
        }

        return $isLoaded;
    }
    
    function save_image($fileData)
    {
        if (isset($fileData['articleImage']))
        {
            move_uploaded_file($fileData['articleImage']['tmp_name'], "images/" . $this->data['articleID'] . "_news.jpg");
        }
    }
        
    function save()
    {
        $isSaved = false;
        // save the data inside of data property
        
        // if data is new, INSERT.  check $this->data['articleID'] to see if it has a value.
        if (empty($this->data['articleID'])) // insert
        {
            $insertSQL = "INSERT INTO newsarticles 
                    (articleTitle, articleContent, articleAuthor, articleDate) 
                    VALUES 
                    (?, ?, ?, ?)";
            
            $stmt = $this->db->prepare($insertSQL);

            if ($stmt)
            {
                $data = $this->data;
                unset($data['articleID']);
                
                if ($stmt->execute(array_values($data)))
                {
                    // store the id back so we can have it beyond this point
                    $this->data['articleID'] = $this->db->lastInsertId();
                    $isSaved = true;
                }
            } 
            else
            {
                $this->errors[] = "Error saving, please contact: ";
            }                
        }
        else // if data is not new, UPDATE
        {
            $updateSQL = "UPDATE newsarticles SET 
                    articleID = ?,
                    articleTitle = ?,
                    articleContent = ?,
                    articleAuthor = ?,
                    articleDate = ?
                WHERE articleID = ?
            ";
            
            $stmt = $this->db->prepare($updateSQL);

            if ($stmt)
            {                
                // NOTE: execute required regular arrays vs associative array
                if ($stmt->execute(array_merge(array_values($this->data), array($this->data['articleID']))))
                {
                    $isSaved = true;
                }
            }                        
            else
            {
                $this->errors[] = "Error saving, please contact: ";
            }    
        }
                
        return $isSaved;
    }

    // validate the $this->data array and store any errors in $this->errors
    function validate()
    {
        $isValid = true;
        
        // validate individual values in $this->data
        if (empty($this->data['articleTitle']))
        {
            $this->errors[] = "Article Title is required";
            $isValid = false;
        }
        else 
        {
            if (strlen($this->data['articleTitle']) > 150)
            {
                $this->errors[] = "Article Title must be less than 150 characters";
                $isValid = false;
            }            
        }
        
        return $isValid;
    }
    
    function getList()
    {
        $selectSQL = "SELECT * FROM newsarticles ORDER BY articleAuthor";
        
        $stmt = $this->db->query($selectSQL);
     
        $articleList = $stmt->fetchAll(PDO::FETCH_ASSOC);
        
        return $articleList;
    }
    
    // exports articles into the file passed as CSV
    function exportArticles($filename)
    {
        $articleList = $this->getList();
        
        // did we get an array and does it have data
        if (is_array($articleList) && count($articleList) > 0)
        {                        
            // open the file for writing, creates or truncates
            $export_file = fopen($filename, "w");
            
            // check to make sure handle is good
            if ($export_file) 
            {
                // export header
                fputcsv($export_file, array_keys($articleList[0]));
                
                // for each article, write to file
                foreach ($articleList as $articleData)
                {
                    fputcsv($export_file, $articleData);
                }
                
                // close up the file
                fclose($export_file);
            }
            
        }                
    }
    
    // imports data from an external file
    function importArticles($filename)
    {
        if (file_exists($filename))
        {
            // open the file for reading
            $import_file = fopen($filename, "r");
            
            $lineCnt = 0;
            $headerArray = array(); // place to store the header
            
            while ($import_file && !feof($import_file))
            {               
                // get a row of data into an array
                $rowData = fgetcsv($import_file);
                
                if ($lineCnt++ > 0) // if this is not the first row
                {
                    // merge the header with the data so we can use further
                    $rowData = array_combine($headerArray, $rowData);
                    
                    // call set so we can use validate/save
                    $this->set($rowData);
                    
                    //var_dump($this->data);
                    
                    if ($this->validate()) 
                    {
                        $this->save();
                    }
                    
                }
                else // we have a header
                {
                     $headerArray = $rowData;
                }
            }
            
            // close up the file
            fclose($import_file);
        }
    }
    
}
?>