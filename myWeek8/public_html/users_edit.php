<?php
require_once('../inc/users.class.php');

// if we are attempting to save 
if (isset($_POST['btnCancel']))
{
    header("location:users_list.php");
    exit;
}

// create an instance of the news article class
$user = new users();

 $errorMsg = '';

// check to see if we have an id to load, if so, load it
// this can be from GET or POST
if (isset($_REQUEST['userID'])) 
{
    // load the article
    $user->load($_REQUEST['userID']);
}

// if we are attempting to save 
if (isset($_POST['btnSave']))
{
    // we dont want the button anymroe
    unset($_POST['btnSave']);
    
    // sanitize the data from post and store into a temporary array
    $dataArray = $user->sanitize($_POST);
    
    // set our internal data array to the posted data
    $user->set($dataArray);
    
    // make sure the data is valid before we can save it
    if ($user->validate())
    {

        // attempt to save
        if ($user->save())
        {
            // if the article saved, now save the image
            $user->save_image($_FILES);
            // once we have saved, move to a success page
            // instead of staying on this page.  this prevents
            // double posting on refresh.
            header("location: users_saved.php");
            exit;
        }
        else
        {
            $errorMsg = "Failed to save";
        }
    }
}

// get the data the display will need.  we dont want to use the business 
// object instance directly.
$formData = $user->data;
$errors = $user->errors;

// see Garitt's week 8 files for function getUserLevels

$userLevels = $user->getUserLevels();

  // see Garritt's week 8 files to fix the empty bullet point showing

if (!empty($errorMsg)) 
{
    $errors[] = $errorMsg;
}

include('../tpl/users_edit.tpl.php');
?>