<?php
require_once('../inc/db.config.php');

class test
{
    var $data;
}

class newsArticles 
{
    
    var $data = array(); // stores the article data 
    var $errors = array(); // stores error information
    var $db = null; // stores a pdo connection
    
    function __construct() 
    {
        $this->connectToDB();
    }
    
    function connectToDB()
    {
        $isConnected = true;
        
        // constructor of PDO always throws an exception
        try {
            $this->db = new PDO(DATABASE_CONNECTION_STRING, DATABASE_USER, DATABASE_PASS, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING));
        } 
        catch (PDOException $e)
        {
            $this->errors[] = 'Failed to connect to database';
            $this->errors[] = $e->getMessage();
            $isConnected = false;
        } 
        
        return $isConnected;
    }
    
    // data array can come from anywhere, but it has to adhere to the naming convention
    // and its keys must be the same names as the database columns.
    // ex. $dataArray['articleID'], $dataArray['articleTitle']
    function set($dataArray)
    {
        $this->data = $dataArray;
    }
    
    // data array can come from anywhere, but it has to adhere to the naming convention
    // and its keys must be the same names as the database columns.
    // ex. $dataArray['articleID'], $dataArray['articleTitle']
    function sanitize($dataArray)
    {
        // apply sanitize rules
        
        return $dataArray;
    }

    function load($articleID)
    {
        $isLoaded = false;
        
        // load from database, store to data property
        $loadSQL = "SELECT * FROM newsarticles WHERE articleID = ?";
        
        $stmt = $this->db->prepare($loadSQL);
        
        if ($stmt)
        {
            if ($stmt->execute(array($articleID)))
            {
                $this->set($stmt->fetch(PDO::FETCH_ASSOC));
                
                $isLoaded = true;
            }
        }

        return $isLoaded;
    }
    
    function save()
    {
        $isSaved = false;
        // save the data inside of data property
        
        // if data is new, INSERT.  check $this->data['articleID'] to see if it has a value.
        if (empty($this->data['articleID'])) // insert
        {
            $insertSQL = "INSERT INTO newsarticles 
                    (articleTitle, articleContent, articleAuthor, articleDate) 
                    VALUES 
                    (?, ?, ?, ?)";
            
            $stmt = $this->db->prepare($insertSQL);

            if ($stmt)
            {
                $data = $this->data;
                unset($data['articleID']);
                
                if ($stmt->execute(array_values($data)))
                {
                    $isSaved = true;
                }
            } 
            else
            {
                $this->errors[] = "Error saving, please contact: ";
            }                
        }
        else // if data is not new, UPDATE
        {
            $updateSQL = "UPDATE newsarticles SET 
                    articleID = ?,
                    articleTitle = ?,
                    articleContent = ?,
                    articleAuthor = ?,
                    articleDate = ?
                WHERE articleID = ?
            ";
            
            $stmt = $this->db->prepare($updateSQL);

            if ($stmt)
            {                
                // NOTE: execute required regular arrays vs associative array
                if ($stmt->execute(array_merge(array_values($this->data), array($this->data['articleID']))))
                {
                    $isSaved = true;
                }
            }                        
            else
            {
                $this->errors[] = "Error saving, please contact: ";
            }    
        }
                
        return $isSaved;
    }

    // validate the $this->data array and store any errors in $this->errors
    function validate()
    {
        $isValid = true;
        
        // validate individual values in $this->data
        if (empty($this->data['articleTitle']))
        {
            $this->errors[] = "Article Title is required";
            $isValid = false;
        }
        else 
        {
            if (strlen($this->data['articleTitle']) > 150)
            {
                $this->errors[] = "Article Title must be less than 150 characters";
                $isValid = false;
            }            
        }
        
        return $isValid;
    }
    
    function getList()
    {
        $selectSQL = "SELECT * FROM newsarticles ORDER BY articleAuthor";
        
        $stmt = $this->db->query($selectSQL);
     
        $articleList = $stmt->fetchAll(PDO::FETCH_ASSOC);
        
        return $articleList;
    }
}



?>