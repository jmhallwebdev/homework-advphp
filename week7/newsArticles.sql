-- phpMyAdmin SQL Dump
-- version 4.3.8
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1:3306
-- Generation Time: Feb 21, 2017 at 03:03 AM
-- Server version: 5.5.41
-- PHP Version: 5.4.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `wdv441_2017`
--

-- --------------------------------------------------------

--
-- Table structure for table `newsarticles`
--

CREATE TABLE `newsarticles` (
  `articleID` int(11) NOT NULL,
  `articleTitle` varchar(100) NOT NULL,
  `articleContent` text NOT NULL,
  `articleAuthor` varchar(100) NOT NULL,
  `articleDate` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `newsarticles`
--

INSERT INTO `newsarticles` (`articleID`, `articleTitle`, `articleContent`, `articleAuthor`, `articleDate`) VALUES
(1, 'First Test Article', 'First article text', 'Eric2', '2017-02-20 00:00:00'),
(2, 'Article test 234', 'This is getting groovy', 'Jeremy', '2017-02-21 00:57:42'),
(3, 'Test New Article 1', 'this is a test of creating a new article', 'GG', '2017-02-20 00:00:00'),
(4, 'test article 34', 'test from add list', 'GG', '2017-02-20 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `newsarticles`
--
ALTER TABLE `newsarticles`
  ADD PRIMARY KEY (`articleID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `newsarticles`
--
ALTER TABLE `newsarticles`
  MODIFY `articleID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;