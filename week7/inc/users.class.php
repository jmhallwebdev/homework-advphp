<?php
require_once('../inc/mydb.config.php');

class users 
{
    
    var $data = array(); // stores the article data 
    var $errors = array(); // stores error information
    var $db = null; // stores a pdo connection

    function __construct() 
    {
        $this->connectToDB();
    }
    
    function connectToDB()
    {
        $isConnected = true;
        
        // constructor of PDO always throws an exception
        try {
            $this->db = new PDO(DATABASE_CONNECTION_STRING, DATABASE_USER, DATABASE_PASS, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING));
        } 
        catch (PDOException $e)
        {
            $this->errors[] = 'Failed to connect to database';
            $this->errors[] = $e->getMessage();
            $isConnected = false;
        } 
        
        return $isConnected;
    }
    
    // data array can come from anywhere, but it has to adhere to the naming convention
    // and its keys must be the same names as the database columns.
    // ex. $dataArray['articleID'], $dataArray['articleTitle']
    function set($dataArray)
    {
        $this->data = $dataArray;
    }
    
    // data array can come from anywhere, but it has to adhere to the naming convention
    // and its keys must be the same names as the database columns.
    // ex. $dataArray['articleID'], $dataArray['articleTitle']
    function sanitize($dataArray)
    {
        // apply sanitize rules

        $dataArray['userName'] = filter_var($dataArray['userName'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);

        $dataArray['userPassword'] = filter_var($dataArray['userPassword'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);

        $dataArray['userLevel'] = filter_var($dataArray['userLevel'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
        
        return $dataArray;
    }

    function load($userID)
    {
        $isLoaded = false;
        
        // load from database, store to data property
        $loadSQL = "SELECT * FROM users WHERE userID = ?";
        
        $stmt = $this->db->prepare($loadSQL);
        
        if ($stmt)
        {
            if ($stmt->execute(array($userID)))
            {
                $this->set($stmt->fetch(PDO::FETCH_ASSOC));
                
                $isLoaded = true;
            }
        }

        return $isLoaded;
    }
    
    function save()
    {
        $isSaved = false;
        // save the data inside of data property
        
        // if data is new, INSERT.  check $this->data['articleID'] to see if it has a value.
        if (empty($this->data['userID'])) // insert
        {
            $insertSQL = "INSERT INTO users 
                    (userName, userPassword, userLevel) 
                    VALUES 
                    (?, ?, ?)";
            
            $stmt = $this->db->prepare($insertSQL);

            if ($stmt)
            {
                $data = $this->data;
                unset($data['userID']);
                
                if ($stmt->execute(array_values($data)))
                {
                    $isSaved = true;
                }
            } 
            else
            {
                $this->errors[] = "Error saving, please contact: ";
            }                
        }
        else // if data is not new, UPDATE
        {
            $updateSQL = "UPDATE users SET 
                    userID = ?,
                    userName = ?,
                    userPassword = ?,
                    userLevel = ?
                WHERE userID = ?
            ";
            
            $stmt = $this->db->prepare($updateSQL);

            if ($stmt)
            {                
                // NOTE: execute required regular arrays vs associative array
                if ($stmt->execute(array_merge(array_values($this->data), array($this->data['userID']))))
                {
                    $isSaved = true;
                }
            }                        
            else
            {
                $this->errors[] = "Error saving, please contact: ";
            }    
        }
                
        return $isSaved;
    }

    // validate the $this->data array and store any errors in $this->errors
    function validate()
    {
        $isValid = true;
        
        // validate individual values in $this->data
        if (empty($this->data['userName']))
        {
            $this->errors[] = "User Name is required";
            $isValid = false;
        }
        else 
        {
            if (strlen($this->data['userName']) > 15)
            {
                $this->errors[] = "User Name must be less than 15 characters";
                $isValid = false;
            }            
        }

        if (empty($this->data['userPassword']))
        {
            $this->errors[] = "User Password is required";
            $isValid = false;
        }
        else 
        {
            if (strlen($this->data['userPassword']) > 15)
            {
                $this->errors[] = "User Password must be less than 15 characters";
                $isValid = false;
            }            
        }

        if (empty($this->data['userLevel']))
        {
            $this->errors[] = "User Level is required";
            $isValid = false;
        }
        else 
        {
            if (strlen($this->data['userLevel']) > 1)
            {
                $this->errors[] = "User Level must be 1 character only";
                $isValid = false;
            }            
        }
        
        return $isValid;
    }

    function getList()
    {
        $selectSQL = "SELECT * FROM users ORDER BY userID";
        
        $stmt = $this->db->query($selectSQL);
     
        $userList = $stmt->fetchAll(PDO::FETCH_ASSOC);
        
        return $userList;
    }

    function getUserLevels()
    {
        return array(
          "1" => "Site User",
          "2" => "News Editor",
          "3" => "Administrator",
          "4" => "Super Administrator"
        );
    }

// check this function below versus Garritt's from week 8 folder - changes needed

    function login($userName, $userPassword)
    {
        $validUser = false;
        $userName = trim($userName);
        $userPassword = trim($userPassword);
        
        $loginSQL = "SELECT userID FROM users WHERE userName = ? AND userPassword = ?";
        
        $stmt = $this->db->prepare($loginSQL);

        if ($stmt)
        {
            if ($stmt->execute(array($userName, $userPassword)))
            {
                $data = $stmt->fetch(PDO::FETCH_ASSOC);
                
                // ensuring we have an array from the fetch and that it has a user id > 0
                if ($data && $data['userID'] > 0)
                {                
                    $userID = $data['userID'];
                }
            }
        }

        // returning the user id so on calling code we can use false or a value > 0 as 
        // indicator of success
        return $userID;        
    }
    
}
?>