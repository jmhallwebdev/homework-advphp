<?php
// the goal of this api page is to return all articles
// or a single article in JSON format

require_once('../inc/users.class.php');

// create an instance of the news article class
$user = new users();

// if we have an article id, then retrieve it
if (isset($_GET['userID']) && $_GET['userID'] > 0)
{
    // load the article
    if ($user->load($_GET['userID']))
    {
        // if successful, show the article in JSON
        echo json_encode($user->data);
    }        
}
else
{
    // get the list of articles
    $usersList = $user->getList();
    // show in JSON
    echo json_encode($usersList);
}
?>