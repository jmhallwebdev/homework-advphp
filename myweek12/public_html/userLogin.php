<?php
session_start();
require_once('../inc/users.class.php');

$user = new users();
$errorMsg = '';
$userID = '';

// check to see if we have an submitted userName and userPassword

if (isset($_REQUEST['userName'], $_REQUEST['userPassword'])) 
{
	$userName = $_REQUEST['userName'];
	$userPassword = $_REQUEST['userPassword'];
        
$userID = $user->checkLogin($userName, $userPassword);
	//try to log the user in
    //$user->login($_REQUEST['userName'], $_REQUEST['userPassword']);
    
    //echo('The following userID is now logged in: ');
    //echo($userID);
}

if ($userID) 
{
    $_SESSION['userID'] = $userID;

    //echo($userID);
    header("location: users_list.php");
    exit;
}

include('../tpl/users_login.tpl.php');
?>