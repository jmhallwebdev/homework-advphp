<?php
require_once('../inc/db.config.php');

class newsArticles 
{
    
    var $data = array(); // stores the article data ( = developer note)
    var $errors = array(); // stores error information ( = developer note)
    var $db = null; // stores a pdo connection ( = developer note)
    
    function connectToDB()
    {
        $isConnected = true;
        
        // constructor of PDO always throws an exception
        try {
            $this->db = new PDO(DATABASE_CONNECTION_STRING, DATABASE_USER, DATABASE_PASS, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING));
        } 
        catch (PDOException $e)
        {
            $this->errors[] = 'Failed to connect to database';
            $this->errors[] = $e->getMessage();
            $isConnected = false;
        } 
        
        return $isConnected;
    }
    
    // data array can come from anywhere, but it has to adhere to the naming convention
    // and its keys must be the same names as the database columns.
    // ex. $dataArray['articleID'], $dataArray['articleTitle']
    function set($dataArray)
    {
        $this->data = $dataArray;
    }
    
    // data array can come from anywhere, but it has to adhere to the naming convention
    // and its keys must be the same names as the database columns.
    // ex. $dataArray['articleID'], $dataArray['articleTitle']
    function sanitize($dataArray)
    {
        // apply sanitize rules
        
        return $dataArray;
    }

    function load($articleID)
    {
        // load from database, store to data property
        
        
    }
    
    function save()
    {
        // save the data inside of data property
        
        // if data is new, INSERT.  check $this->data['articleID'] to see if it has a value.
        
        // if data is not new, UPDATE
        
        
    }

    // validate the $this->data array and store any errors in $this->errors
    function validate()
    {
        $isValid = true;
        
        // validate individual values in $this->data
        
        
        return $isValid;
    }
    
    
}



?>